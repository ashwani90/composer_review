<?php

namespace AppBundle\Repository;

/**
 * OperatorRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OperatorRepository extends \Doctrine\ORM\EntityRepository
{
    public function findElWithNetID($net_id)
    {
        $qb = $this->createQueryBuilder('p')
            ->Where('p.networkId = :net_id')
            ->setParameter('net_id',$net_id)
            ->getQuery();
        return $qb->execute();


    }
}
