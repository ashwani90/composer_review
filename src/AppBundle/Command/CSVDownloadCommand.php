<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 13/6/18
 * Time: 11:47 AM
 */

namespace AppBundle\Command;
use AppBundle\Service\CSVUploadService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
class CSVDownloadCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('csv-upload')
            ->setDescription("Uploads the CSV data")
            ->setHelp("This command allows you to upload the csv to the database")
            ->addArgument("filename", InputArgument::REQUIRED, "This is the name of the file to be uploaded")
            ->addArgument("networkID", InputArgument::REQUIRED, "This is the network Id of the operator")
            ->addArgument("expirydate", InputArgument::REQUIRED, "This is the expirydate of the vouchers");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('filename');
        $networkId = $input->getArgument("networkID");
        $expirydate = $input->getArgument("expirydate");
        $csv = $this->parseCSV($filename);
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getEntityManager();
        $csvuploadservice = new CSVUploadService($em);
        $resp = $csvuploadservice->uploadCSV($csv,$networkId,$expirydate);
        $output->writeln($resp['status']);
    }
    private function parseCSV($filename)
    {
        $ignoreFirstLine = true;
        $fileDir = dirname($filename);
        $fileName = basename($filename);

        $finder = new Finder();
        $finder->files()
            ->in($fileDir)
            ->name($fileName)
        ;
        foreach ($finder as $file) { $csv = $file; }

        $rows = array();
        if (($handle = fopen($csv->getRealPath(), "r")) !== FALSE) {
            $i = 0;
            while (($data = fgetcsv($handle, null, ";")) !== FALSE) {
                $i++;
                if ($ignoreFirstLine && $i == 1) { continue; }
                $rows[] = $data;
            }
            fclose($handle);
        }

        return $rows;
    }
}
