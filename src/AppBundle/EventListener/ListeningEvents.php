<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 15/6/18
 * Time: 10:36 AM
 */

namespace AppBundle\EventListener;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class ListeningEvents
{
    private $api_token;
    public function __construct($api_token,LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->api_token = $api_token;
    }
    public function onKernelRequest(GetResponseEvent $event)
    {

        $api_key = $event->getRequest()->headers->get('Authorization');
        
        if($api_key != $this->api_token)
        {
            $response = new Response();
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            $response->setContent("Invalid Credentials");
            $event->setResponse($response);
        }else{

        }
    }
}